const express = require('express')
const router = new express.Router()
const { spawn, exec} = require('child_process')
const fs = require('fs')
const path = require('path')
const {autoDir} = require('../../config')
const logger = require('../logger')

const readFile = (filePath) => {
    if (fs.existsSync(filePath)) {
      return String(fs.readFileSync(filePath)).trim()
    }
    return ''
  }
  
  const checkTrackFile = async (filePath) => {
    const data = readFile(filePath)
    if (data) {
      return data
    }
    await timeoutPromise(1000)
    return await checkTrackFile(filePath)
  }
  
const timeoutPromise = (time) => new Promise((res) => setTimeout(res, time))

router.post('/prepare', async (req, res, next) => {
    logger.info('Prepare to login...')
    const id = new Date().getTime()

    const command = `docker run -e CYPRESS_ID=${id} -dit --name ${id}  -v ${autoDir}:/e2e -w /e2e cypress/included:4.11.0`
    exec(command)

    logger.info('Already run command ', command, id)

    res.status(200).json({id, status: 'OK'})
})

const waitLogin = async (id) => {
    await timeoutPromise(1000)
    let status = await checkTrackFile(`${autoDir}/data/${id}.track`)
    if (status.includes('ERROR') || status === 'WAIT_FOR_CODE' || status === 'LOGIN_SUCCESS') {
        return status
    }
    return await waitLogin(id)
}

const waitFile = async (filePath) => {
    if (fs.existsSync(filePath)) {
        return
    }
    await timeoutPromise(1000)
    return await waitFile(filePath)
}

router.post('/login', async (req, res, next) => {
    const {username, password, id} = req.body
    logger.info('Receive data', username, password, id)

    await waitFile(path.join(autoDir, 'data', `${id}.json`))
    fs.writeFileSync(path.join(autoDir, 'data', `${id}.json`), `${JSON.stringify({username, password, code: ''})}\n`)
    logger.info('Already write file')

    let status = await waitLogin(id)

    logger.info('Got status ', status)
    res.status(200).json({username, id, status})
})

const waitCode = async (id) => {
    await timeoutPromise(1000)
    let status = await checkTrackFile(`${autoDir}/data/${id}.track`)
    if (status === 'LOGIN_SUCCESS') {
        return status
    }
    return await waitCode(id)
}

router.post('/code', async (req, res, next) => {
    const {id, code} = req.body
    logger.info('Receive code ', code)

    fs.writeFileSync(`${autoDir}/data/${id}.json`, `${JSON.stringify({code})}\n`)

    const status = await waitCode(id)

    logger.info('Got status ', status)
    res.status(200).json({id, status})
})

router.post('/cancel', async (req, res, next) => {
    const {id} = req.body

    const command = `sudo docker rm -f ${id}`
    spawn(command, {shell: true})

    res.status(200).json({id, status: 'OK'})
})

module.exports = router
