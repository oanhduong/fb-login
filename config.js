const hasDbAuthen = process.env.DB_USER && process.env.DB_PWD
module.exports = {
  port: {
    http: process.env.PORT || 3000
  },
  log: {
    dir: 'logs',
    filename: process.env.LOG_FILE_NAME || 'events.log',
    size: process.env.LOG_FILE_SIZE || 10485760,
    level: process.env.LOG_LEVEL || 'debug' // trace > debug > info > warn > error > fatal
  },
  autoDir: process.env.AUTO_DIR || '~/dg-auto'
}