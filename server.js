global.BASE_DIR = __dirname

const http = require('http')
const express = require('express')
const config = require('./config')
const logger = require('./modules/logger')
const cors = require('cors')

const app = express()
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: false}))

app.use('/api/v1/auth', require('./modules/login').router)

app.use((err, req, res, _next) => res.status(500).json({ message: err.message || String(err) }))

const server = http.createServer(app)

server.listen(config.port.http, (err) => {
  if (err) {
    logger.error(`Start server error`, config, err)
  } else {
    logger.info(`Server is listening on port ${config.port.http}`)
  }
})

